import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ['Mr.', 'Mrs.', 'Miss.']
    result = []

    for a in titles:
        df_title = df[df['Name'].str.contains(a)]
        missing = df_title['Age'].isnull().sum()
        medians = round(df_title['Age'].median())
        df.loc[df['Name'].str.contains(a) & df['Age'].isnull(), 'Age'] = medians
        result.append((a, missing, medians))

    return result
